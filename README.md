# EditorDemo

Create a new angular workspace through
```angular2
ng new projectViewDemo
```

Create the hosting component the projectView lives in:
```angular2
ng generate component projectView
```

and include it in the app component
```angular2
<app-project-view></app-project-view>
```

We want the projectView to contain the project details component, the team component and the tasks component. 
The project view component hosts the other components and has the project information with it (for now just a string for its name).
```angular2
private projectName: string;
```

Now we want the view to display the name of the project.
Since it is a string, we want interpolation:
```angular2
{{projectName}}
```

The projectView has three components, lets start with the details component:
```angular2
ng generate component projectDetails
<app-project-details></app-project-details>
```

The project details have some structure and look as follows:
```angular2
<div class=".detailsCard">
<h2>Project Name</h2>
<p>Some project text</p>
<h4>Goal</h4>
<p>Some more text</p>
<button class="button">Join</button>
</div>
```

We want project name to be taken from the parent component: Input
```angular2
Child: @Input() name: string;
Parent: [name]="projectName"
```

Lets take care of the button doing something when clicking, by acknowledging your click
```angular2
HTML: (click)="joinProject()"
TS: private joinProject(){
        console.log("User wants to join the project");
      }
``` 

Now lets give the user feedback for the success of joining the project
```angular2
private joined = false;
private jointAcklowledgement = '';
In joinProject:
this.joined = true;
this.userFeedback = ("You joined the project "+this.name);
```

Lets also reflect that in the button: disable after joining: property binding
```angular2
<button class="button" (click)="joinProject()" [disabled]="joined">Join</button>
```

Finally, lets make the user tell the project people why the want to join: [(ngModel)]
For this we need to import the forms module in the app module:
```angular2
import { FormsModule } from '@angular/forms';
imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ]
```
in the app.module.

Now we are ready for the two-way binding:
```angular2
<input [(ngModel)]="motivation">
<div>
    Your motivation to join {{name}}:
    <p>
        {{motivation}}
    </p>
</div> 
```

Lets make sure the user writes at least 40 characters in the motivation in order to allow them.
```angular2
if(this.motivation.length >= 40){
      this.joined = true;
      this.userFeedback = ("You joined the project "+this.name);
    }
```

Also make sure that user needs to write more when they don't write enough, that they are ready to join and they have joined the project.
For this, make the userFeedback more dynamic, and check if something changed:
```angular2
private motivationChange() {
    if(this.joined){
      this.userFeedback = ("You joined the project "+this.name);
    } else if (this.motivation.length < 40){
      this.userFeedback = "You need to write at least 40 characters about your motivation"
    } else {
      this.userFeedback = "You are ready to join!";
    }
  }

private joinProject(){
    console.log("User wants to join the project");
    if(this.motivation.length >= 40){
      this.joined = true;
      this.motivationChange(); 
    }
  }
```

Hide the button when the motivation is not sufficient:
```angular2
<button class="button" (click)="joinProject()" [disabled]="joined || (motivation.length < 40)">Join</button>
```

We want the project to know that the user has joined in order to guide the other components: Output
```angular2
In child:
@Output() newUserJoin = new EventEmitter<string>();
this.newUserJoin.emit('User joined project');
In parent:
<app-project-details [name]="projectName" (newUserJoin)="userJoined($event)"></app-project-details>
```

Instead of disabling the button and showing the irrelevant stuff, hide it: ngIf
```angular2
<div *ngIf="!joined">
    <button class="button" (click)="joinProject()" [disabled]="joined || (motivation.length < 40)">Join</button>
    {{userFeedback}}
    <input [(ngModel)]="motivation" (ngModelChange)="motivationChange()">
    <div>
        Your motivation to join {{name}}:
        <p>
            {{motivation}}
        </p>
    </div> 
    </div>
</div>
```

Now create the team component to show the team members
```angular2
ng generate component ProjectTeam
```
and put the team information in

Put in some mock data for the team (with just the names)
```angular2
private teamMembers: string[] = ['Hans', 'Josef', 'Felix'];
```

generate the component for the team members:
```angular2
ng generate component teamMember
```

Iterate the team member component for each member in the team: *ngFor
```angular2
<app-team-member *ngFor="let member of teamMembers">{{member}}</app-team-member>
```

Bind the name of the member within the respective component
```angular2
<app-team-member *ngFor="let member of teamMembers" [teamMember]="member">{{member}}</app-team-member>
@Input() teamMember: string;
```

Make the members of the team a Set
```angular2
private teamMembers: Set<String>;
constructor() { 
    this.teamMembers = new Set<String>(['Hans', 'Josef', 'Felix']);
  }
```

Make the user being added to the team when they join; set the user in the context
```angular2
In parent:
private sessionUserName: string = '';
<app-project-team [newMember]="sessionUserName"></app-project-team>
private userJoined(){
    console.log("A user has joined the project");
    this.sessionUserName = "tommy";
  }
In child:
@Input() newTeamMember: string;  
ngOnChanges(){
    if((this.newTeamMember !== '') && (!this.teamMembers.has(this.newTeamMember))){
      this.teamMembers.add(this.newTeamMember);
    }
  }
```

Create the task component
```angular2
ng generate component ProjectTasks
<app-project-task></app-project-task>
```

Set up selectively displaying the respective component
```angular2
<div (click)="selectFocusedComponent('starter')">Starter</div>
<div (click)="selectFocusedComponent('entree')">Entree</div>
<div (click)="selectFocusedComponent('desert')">Desert</div>
{{focusedTaskComponent}}
private focusedTaskComponent='';
private selectFocusedComponent(focusedComponent: string): void {
    this.focusedTaskComponent = focusedComponent;
  }
```

Create the starter, entree & main component (and 404 component)
```angular2
ng generate component StarterTasks
ng generate component EntreeTasks
ng generate component DesertTasks
ng generate component Four0Four
```

Conditionally show the respective component
```angular2
<div [ngSwitch]="focusedTaskComponent">
<div (click)="selectFocusedComponent('starter')">Starter</div>
<app-starter-tasks *ngSwitchCase="'starter'"></app-starter-tasks>
<div (click)="selectFocusedComponent('entree')">Entree</div>
<app-entree-tasks *ngSwitchCase="'entree'"></app-entree-tasks>
<div (click)="selectFocusedComponent('desert')">Desert</div>
<app-desert-tasks *ngSwitchCase="'desert'"></app-desert-tasks>
<app-four-ofour-component *ngSwitchDefault></app-four-ofour-component>
</div>
```

Mark the user team member in red: ngClass
```angular2
HTML:
<div [ngClass]="(teamMember === 'tommy') ? 'selected-member' : non-selected-member">
{{teamMember}}
</div>
CSS:
.selected-member{
    color: red;
}

.non-selected-member{
    color: black;
}
```

Put the project members into a service: declare the member provision service
```angular2
ng generate service MemberProvision
```

put the array in the service and access it from the member component:
```angular2
service:
private teamMembers = new Set<String>(['Hans', 'Josef', 'Felix']);
  public getTeamMembers(): Set<String>{
    return this.teamMembers;
  }
ProjectTeamComponent:
  private teamMembers: Set<String>;
  constructor(private memberService: MemberProvisionService) { 
    this.teamMembers = memberService.getTeamMembers();
  }
```

Add the new team member to the service
```angular2
ProjetViewComponent:
constructor(private memberService: MemberProvisionService) {
this.memberService.addMember(this.sessionUserName);
member provision service:
  public addMember(memberToAdd: string){
    this.teamMembers.add(memberToAdd);
  }
```